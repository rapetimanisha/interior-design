import { Component } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { InteriorService } from './../interior.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent {
  formData: any = {};

  constructor(private interiorService: InteriorService, private _snackBar: MatSnackBar) {}

  submitForm() {
    console.log(this.formData); 

    this.create();

    this._snackBar.open('Thank you for choosing our services!', 'Close', {
      duration: 5000, 
      horizontalPosition: 'center', 
      verticalPosition: 'top'
    });

    this.formData = {};
  }

  create() {
    this.interiorService.create(this.formData).subscribe(
      (res: any) => {
        console.log(res,'hi')
        const details = res.result;
        console.log(details, "mannnnnnnnnnnnnnnnnn");
      },
      (error: any) => {
        console.error('Submission error', error);
      }
    );
  }
}
