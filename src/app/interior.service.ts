import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from './../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class InteriorService {
  submitForm(formData: any) {
    throw new Error('Method not implemented.');
  }
  

  constructor(private http: HttpClient) { }

  signup(userData: any): Observable<any> {
    return this.http.post(`${environment.apiUrl}/user/created_register_Details`,userData);
  }

  login(data: any){
    console.log(data, "manishaaaaaaaa")
    return this.http.post(`${environment.apiUrl}/user/userlogin`,data)
  }
  create(data:any): Observable<any>{
    return this.http.post(`${environment.apiUrl}/contact/create_contact_details`,data)
  }
}
