import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor() {}
  private isAuthenticated = false;
  isLoggedIn(): boolean {
    return this.isAuthenticated;
    return !!localStorage.getItem('userId');
  }

  login(token: string): void {
    this.isAuthenticated = true;
    localStorage.setItem('userId', token);
  }

  logout(): void {
    this.isAuthenticated = false;
    localStorage.removeItem('userId');
  }
}
