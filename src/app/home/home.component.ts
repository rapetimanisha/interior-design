import { Component } from '@angular/core';
import { TitleService } from '../title.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  constructor(private titleService: TitleService) { }

  ngOnInit(): void {
    this.titleService.setTitle('GLM VIZAG INTERIORS');
  }
}
