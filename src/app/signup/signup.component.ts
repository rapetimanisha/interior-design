import { AuthService } from './../auth.service';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { InteriorService } from '../interior.service';


interface signupForm {
  email: string;
  password: string;
  mobilenumber: string;
  username: string;
}

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent {
  email!: string;
  password!: string;
  username!: string;
  mobilenumber!: string;
  login: signupForm = { email: '', password: '', mobilenumber: '', username: '' };

  constructor(private interiorService: InteriorService, private _Router: Router, private AuthService:AuthService) {}

  onSubmit(signupForm: any) {
    if (signupForm.valid) {
      console.log('Form Data:', signupForm.value); // Log form data to the console

      this.interiorService.signup(signupForm.value).subscribe(
        response => {
          if (response && response.result) {
            console.log('Signup successful', response);
            this._Router.navigate(['/login']);
          } else {
            console.error('Signup failed', response);
          }
        },
        error => {
          console.error('Signup error', error);
        }
      );
    }
  }
}
