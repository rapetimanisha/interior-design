import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { InteriorService } from '../interior.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatSnackBar } from '@angular/material/snack-bar';
// import { AuthService } from './../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  email: string = '';
  password: string = '';
  userId: any;
  showSpinner: boolean = false; // Flag to show/hide spinner
  hideSpinner: boolean=false;

  constructor(
    private router: Router,
    private interiorService: InteriorService,
    private spinner: NgxSpinnerService ,
    private _snackBar: MatSnackBar
   
  ) {}

  goToSignup() {
    this.router.navigate(['/signup']);
  }

  login() {
    this.showSpinner = true; // Show spinner when login button is clicked

    const userData = { email: this.email, password: this.password };
    this.interiorService.login(userData).subscribe(
      (res: any) => {
        const logindetails = res.result.user;
        console.log(logindetails, "maniiiii");
        this.userId = logindetails.id;
        localStorage.setItem("userId", this.userId);
        setTimeout(() => {
          this.router.navigate(['/home']);
        }, 1000); 
        this._snackBar.open('login  successfull!', 'Close', {
          duration: 5000, 
          horizontalPosition: 'center', 
          verticalPosition: 'top'
        });
        // Navigate to home or dashboard after successful login
       
      },
      (error) => {
        console.error('Login error', error);
      }
    ).add(() => {
      this.hideSpinner = true; // Hide spinner after login attempt completes
    });
  }

  loginWithGoogle() {
    // Implement login with Google functionality
    throw new Error('Method not implemented.');
  }
}
